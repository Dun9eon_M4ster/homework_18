#include <iostream>
using namespace std;

class Stack
{
public:
	/*Stack(int _a) : a(_a)
	{}*/

	Stack(int c)
	{
		int* b = new int[c];
		Amount = c;
		for (int i = 0; i < c; i++)
		{
			cout << "Enter value: ";
			cin >> b[i];
			cout << endl;
			LastElem = &b[i];
		}
		FirstElem = &b[0];
	}

	void Push(int c)
	{
		Amount++;							//���������� ���������� ���������� �� �����������
		int* NewArray = new int[Amount];
		int* Count = FirstElem;
		for (int i = 0; i < Amount - 1; i++)
		{
			NewArray[i] = Count[i];
		}
		NewArray[Amount - 1] = c;
		cout << endl;

		delete [] FirstElem;				//������� ������ � �������������� ��������� �� ������ �������
		FirstElem = &NewArray[0];
		NewArray = nullptr;
	}
	int Pop()
	{
		Amount--;							//���������� ���������� ���������� �� �����������
		int ReturningValue = 0;
		int* NewArray = new int[Amount];
		int* Count = FirstElem;
		for (int i = 0; i < Amount; i++)
		{
			NewArray[i] = Count[i];
		}
		ReturningValue = Count[Amount];
		delete[] FirstElem;					//������� ������ � �������������� ��������� �� ������ �������
		FirstElem = &NewArray[0];
		NewArray = nullptr;
		return ReturningValue;
	}
	void Show()
	{
		int* Count = FirstElem;
		for (int i = 0; i < Amount; i++)
		{
			cout << Count[i] << " ";
		
		}
		cout << endl;
	}
private:
	int* FirstElem = nullptr;
	int* LastElem = nullptr;
	int Amount = 0;
};

int main()
{
	int a;
	cout << "Enter size of stack: ";
	cin >> a;
	cout << endl;
	Stack stack(a);
	stack.Show();
	stack.Push(69);
	stack.Show();
	stack.Push(146);
	stack.Show();
	cout <<"Last element is: "<< stack.Pop() << endl;
	stack.Show();
	cout <<"Last element is: "<< stack.Pop() << endl;
	stack.Show();
	cout << "Last element is: " << stack.Pop() << endl;
	stack.Show();
	return 0;
}